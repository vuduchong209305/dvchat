'use strict'

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

export const store = new Vuex.Store({
	strict: true,
	state: {
		socket : getCookie('token-socket') != '' ? new WebSocket('ws://apichat.licham.vn/socket', getCookie('token-socket')) : '',
		userData : JSON.parse(localStorage.getItem("_isMe")),
		forward_msg : null,
		reply : null,
		roomCurrent : null,
		groupDetail: null,
		listMessage : [],
		page : 0,
		allMedia: [],
		infoAddRoomPrivate : null,
		listRooms: [],
		showGroupPrivate : false
	},
	mutations: {
		reply: (state, payload) => {
			if(payload.status == 'close-reply') {
				state.reply = null
			} else {
				state.reply = payload
			}
		},
		forward: (state, payload) => {
		    state.forward_msg = payload
		},
		roomCurrent: (state, payload) => {
			state.roomCurrent = payload
		},
		groupDetail: (state, payload) => {
			state.groupDetail = payload
		},
		listMessage: (state, payload) => {
			if(payload.status == 1) { // Có tin nhắn mới
				
				if(payload.data.group_id == state.roomCurrent) {
					const listMembers = JSON.parse(localStorage.getItem('listMembers'))

					for(let i = 0; i < listMembers.length; i++) {
						if(listMembers[i]._id == payload.data.user_id) {
							payload.data.name = listMembers[i].name
							payload.data.avatar = listMembers[i].avatar
						}
					}

					state.listMessage.unshift(payload.data)
				}
				
			} else if(payload.status == 2) { // Có reaction mới
				const index = state.listMessage.findIndex(e => { // Xác định vị trí của message
					return (e._id == payload.data.message_id && e.group_id == payload.data.group_id)
				})

				const position = state.listMessage[index].reacted.findIndex(e => { // Xác định vị trí reaction của user
					return e.user_id == payload.data.user_id
				})

				if(position != -1) { // Nếu đã tồn tại user reaction đó thì replace content reaction
					state.listMessage[index].reacted[position].content = payload.data.content
				} else {
					state.listMessage[index].reacted.push(payload.data) // Push vào mảng reacted
				}

			} else {
				state.listMessage = payload
			}
		},
		incrementPage: (state, payload) => {
			state.page += payload
		},
		allMedia: (state, payload) => {
			state.allMedia = payload
		},
		infoAddRoomPrivate: (state, payload) => {
			state.infoAddRoomPrivate = payload
		},
		listRooms: (state, payload) => {
			if(payload.status == 'add-new-room') { // Add room vào list room đang có

				state.listRooms.push(payload)

			} else if(payload.status == 'sort') { // Sắp xếp thứ tự room khi có tin nhắn mới

				const index = state.listRooms.findIndex((e) => { // Xác định vị trí của room hiện tại trong danh sách room
					return e._id == payload.data.group_id
				})

				const room_current = state.listRooms.splice(index, 1) // Lấy room đó ra
				room_current[0].last_message = payload.data // Thay thế last_message mới cho room đó
				state.listRooms.unshift(room_current[0]) // Push room đó vào đầu list room

			} else { // Khởi tạo & sắp xếp khi mới vào web

				state.listRooms = payload.sort((a, b) => {
					if (!b.last_message) return 1;
					if (!a.last_message) return 1;
					return new Date(b.last_message.created_at) - new Date(a.last_message.created_at)
				});
			}
		},
		showGroupPrivate: (state, payload) => {
			state.showGroupPrivate = payload
		}
	},
	actions: {
		listMessage: (context, payload) => {
			context.commit('listMessage', payload)

			payload.status = 'sort' // Tạo 1 status sắp xếp
			context.commit('listRooms', payload)
		},
		listRooms: (context, payload) => {
			payload.status = 'add-new-room' // Tạo 1 status add thêm nhóm vào danh sách nhóm
			context.commit('listRooms', payload)
		},
		reaction: (context, payload) => {
			context.commit('listMessage', payload)
		},
		reply: (context, payload) => {
			context.commit('reply', payload)
		}
	},
	getters: {

	},
	setters: {

	}
})