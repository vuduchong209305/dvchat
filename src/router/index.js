/* eslint-disable */
import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/views/Login';
import Chat from '@/views/Chat';
import ChatDetail from '@/components/ChatDetail';
import Register from '@/views/Register';
import ForgotPassword from '@/views/ForgotPassword';
import Page404 from '@/views/Page404';

Vue.use(Router);
export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/forgotpassword',
            name: 'ForgotPassword',
            component: ForgotPassword
        },
        {
            path: '/',
            name: 'Chat',
            component: Chat,
            children: [
                {
                    name : 'ChatDetail',
                    path: ':id',
                    component: ChatDetail
                }
            ]
        },
        {   
            path: '*',
            component: Page404
        }
    ]
})