import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Notifications from 'vue-notification'
import axios from 'axios'
import VueLodash from 'vue-lodash'
import lodash from 'lodash'
import { store } from './vuex/store'
import VueChatScroll from 'vue-chat-scroll'
import moment from 'vue-moment'
import mixin from './mixins/mixin.js'

Vue.config.productionTip = false

Vue.use(moment);
Vue.use(VueChatScroll)
Vue.use(Notifications)
Vue.use(VueLodash, { name: 'custom' , lodash: lodash })

Vue.prototype.$_baseServer = "http://apichat.licham.vn/api/"
Vue.prototype.$_baseMedia = "http://apichat.licham.vn/media/"

axios.defaults.withCredentials = true;

Vue.use({
    install (Vue) {
		Vue.prototype.$api = axios.create({
			baseURL: Vue.prototype.$_baseServer
		})
	}
})

Vue.mixin(mixin); 

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
