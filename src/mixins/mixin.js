export default {
	methods: {
		notify(type = 'success', text = 'success') {
			this.$notify({
                type: type,
                title: 'Thông báo',
                text: text
            });
		}
	},
	filters: {
		reacted(value) {
			switch(parseInt(value)) {
				case 1:
					return '/img/like.gif';
				break;
				case 2:
					return '/img/love.gif';
				break;
				case 3:
					return '/img/haha.gif';
				break;
				case 4:
					return '/img/wow.gif';
				break;
				case 5:
					return '/img/sad.gif';
				break;
				default:
					return '/img/angry.gif';
			}
		}
	}
}